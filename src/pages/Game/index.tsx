import React, { SFC } from 'react';
import { View } from 'react-native';
import GameEngine from '@src/components/GameEngine';

const Game: SFC = () => (
  <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <GameEngine />
  </View>
);

export default Game;
