// my-theme.ts
import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
  colors: {
    main: '#F4F5F6',
    secondary: '#2EC4B6',
    tertiary: '#CBF3F0',
    quaternary: '#FFBF69',
    quinary: '#FF9F1C',
  },
};

export default theme;
