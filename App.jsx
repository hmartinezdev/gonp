import React from 'react';
import Index from './src/pages';
import { useFonts } from '@use-expo/font';
import { AppLoading } from 'expo';
import { PressStart2P_400Regular } from '@expo-google-fonts/press-start-2p';

const App = () => {
  const [fontsLoaded] = useFonts({
    PressStart2P_400Regular,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  
  return <Index />;
};

export default App;
