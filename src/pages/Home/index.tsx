import React, { SFC } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '..';
import CustomButton from '../../components/Button';
import styled from '@styled/native';

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Home'>;

type HomeProps = {
  navigation: HomeScreenNavigationProp;
};

const HomeView = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  background-color: ${(props) => props.theme.colors.main};
`;

const Title = styled.Text`
  font-family: 'PressStart2P_400Regular';
  font-size: 70px;
  color: ${(props) => props.theme.colors.secondary};
  justify-self: flex-start;
  margin-bottom: 50px;
`;

const Bar = styled.View`
  width: 10px;
  height: 100%;
  position: absolute;
  background-color: ${(props) => props.theme.colors.quinary};
`;

const LeftBar = styled(Bar)`
  left: -20px;
`;

const RightBar = styled(Bar)`
  right: -20px;
`;

const RightBarDisabled = styled(RightBar)`
  opacity: 0.4;
`;

const LeftBarDisabled = styled(LeftBar)`
  opacity: 0.4;
`;

const Home: SFC<HomeProps> = ({ navigation }) => (
  <HomeView>
    <Title>GONP</Title>
    <CustomButton title="Solo" onPress={() => navigation.navigate('Game')}>
      <LeftBar />
      <RightBarDisabled />
    </CustomButton>
    <CustomButton
      title="Multiplayer"
      onPress={() => navigation.navigate('Game')}>
      <LeftBar />
      <RightBar />
    </CustomButton>
    <CustomButton
      title="Options"
      onPress={() => navigation.navigate('Options')}>
      <LeftBarDisabled />
      <RightBarDisabled />
    </CustomButton>
  </HomeView>
);

export default Home;
