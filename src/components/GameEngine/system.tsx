import { TouchEvent } from 'react-native-game-engine';

export interface Entity {
  position: Array<number>;
  rerender: JSX.Element;
}

const MoveFinger = (
  entities: Entity[],
  { touches }: { touches: TouchEvent[] },
): Entity[] => {
  //-- I'm choosing to update the game state (entities) directly for the sake of brevity and simplicity.
  //-- There's nothing stopping you from treating the game state as immutable and returning a copy..
  //-- Example: return { ...entities, t.id: { UPDATED COMPONENTS }};
  //-- That said, it's probably worth considering performance implications in either case.
  touches
    .filter((t) => t.type === 'move')
    .forEach((t: TouchEvent) => {
      const finger = entities[t.id];
      if (finger && finger.position) {
        finger.position = [
          finger.position[0] + (t.delta?.pageX || 0),
          finger.position[1] + (t.delta?.pageY || 0),
        ];
      }
    });

  return entities;
};

export { MoveFinger };
