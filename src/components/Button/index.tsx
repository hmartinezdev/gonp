import React, { SFC } from 'react';
import styled from '@styled/native';

const ButtonContainer = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  width: 240px;
  height: 60px;
  margin: 30px 0;
  padding: 12px;
  border-radius: 0;
  background-color: ${(props) => props.theme.colors.quinary};
  position: relative;
`;

const ButtonText = styled.Text`
  font-size: 19px;
  color: ${(props) => props.theme.colors.tertiary};
  line-height: 14px;
  text-align: center;
  font-family: 'PressStart2P_400Regular';
`;

export interface HomeButtonProps {
  title: string;
  onPress: () => void;
  children?: JSX.Element | JSX.Element[];
}

const HomeButton: SFC<HomeButtonProps> = ({ title, onPress, children }) => (
  <ButtonContainer onPress={onPress}>
    <ButtonText>{title}</ButtonText>
    {children}
  </ButtonContainer>
);

export default HomeButton;
