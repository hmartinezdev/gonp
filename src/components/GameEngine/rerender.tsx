import React, { FC } from 'react';
import styled from '@styled/native';

const RADIUS = 20;

export interface FingerViewProps {
  x: number;
  y: number;
}

const FingerView = styled.View<FingerViewProps>`
  border-color: #ccc;
  border-width: 4px;
  border-radius: ${RADIUS * 2}px;
  width: ${RADIUS * 2};
  height: ${RADIUS * 2};
  background-color: ${(props) => props.theme.colors.secondary};
  position: 'absolute';
  left: ${(props) => props.x}px;
  top: ${(props) => props.y}px;
`;

export interface FingerProps {
  position?: number[];
}

const Finger: FC<FingerProps> = ({ position = [] }) => {
  const x = position[0] - RADIUS / 2;
  const y = position[1] - RADIUS / 2;
  return <FingerView x={x} y={y} />;
};

export { Finger };
