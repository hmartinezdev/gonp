import React from 'react';
import Home from './Home';
import Game from './Game';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider } from '@styled/native';
import theme from '@styled/theme/default';

export type RootStackParamList = {
  Home: undefined;
  Game: undefined;
  Options: undefined;
};

const RootStack = createStackNavigator<RootStackParamList>();

export default function App(): JSX.Element {
  return (
    <ThemeProvider theme={theme}>
      <NavigationContainer>
        <RootStack.Navigator initialRouteName="Home">
          <RootStack.Screen
            name="Home"
            component={Home}
            options={{ headerShown: false }}
          />
          <RootStack.Screen
            name="Game"
            component={Game}
            options={{ headerShown: false }}
          />
          <RootStack.Screen
            name="Options"
            component={Game}
            options={{ headerShown: false }}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
}
